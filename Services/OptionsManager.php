<?php

namespace ThinkCreative\CoreBundle\Services;

use ThinkCreative\CoreBundle\Classes\OptionsManager as Manager;

class OptionsManager
{

    protected $SchemaList = array();
    protected $HandlerList = array();

    public function appendSchema($schema, $configuration) {
        if(
            isset($this->SchemaList[$schema])
        ) {
            $this->SchemaList[$schema]->appendSchema($configuration);
            return true;
        }
        return false;
    }

    public function createHandler($schema, $options) {
        if(
            isset($this->SchemaList[$schema])
        ) {
            $OptionsHandler = $this->SchemaList[$schema]->process($options);
            $this->HandlerList[$OptionsHandler->getHandlerID()] = $OptionsHandler;
            return $OptionsHandler;
        }
    }

    public function createSchema($name, $configuration) {
        if(
            !$this->hasSchema($name)
        ) {
            $this->SchemaList[$name] = new Manager\OptionsSchema(
                $this, $configuration
            );
            return true;
        }
        return false;
    }

    public function getHandler($id) {
        return (
            isset($this->HandlerList[$id]) ? $this->HandlerList[$id] : false
        );
    }

    public function getSchema($name) {
        return (
           (is_string($name) && $this->hasSchema($name)) ? $this->SchemaList[$name] : false
        );
    }

    public function hasSchema($name) {
        return isset($this->SchemaList[$name]);
    }

}
