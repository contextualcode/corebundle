<?php

namespace ThinkCreative\CoreBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\Yaml\Yaml;

class ThinkCreativeCoreExtension extends Extension implements PrependExtensionInterface
{

    public function load(array $configurations, ContainerBuilder $container) {
        $YamlFileLoader = new YamlFileLoader(
            $container, new FileLocator(__DIR__ . '/../Resources/config')
        );
        $YamlFileLoader->load('parameters.yml');
        $YamlFileLoader->load('services.yml');
    }

    public function getAlias() {
        return 'think_creative_core';
    }

    public function prepend(ContainerBuilder $container) {
        $this->prependExtensionConfigurations(
            array_keys(
                $container->getExtensions()
            ),
            $this->getBundleDirectories(
                $container->getParameter('kernel.bundles')
            ),
            function($alias, $configuration) use ($container) {
                $container->prependExtensionConfig(
                    $alias, $configuration
                );
            }
        );
    }

    protected function getBundleDirectories($kernel_bundles) {
        $BundleDirectories = array();
        foreach(
            array_reverse($kernel_bundles) as $BundleName => $BundleClassName
        ) {
            $Reflector = new \ReflectionClass($BundleClassName);
            $BundleDirectories[] = dirname(
                $Reflector->getFileName()
            );
        }
        return $BundleDirectories;
    }

    protected function prependExtensionConfigurations($extensions, $bundle_directories, $callback) {
        foreach($bundle_directories as $BundleDirectory) {
            if(
                file_exists(
                    $ConfigurationDirectory = "$BundleDirectory/Resources/config/_extensions"
                )
            ) {
                foreach($extensions as $ExtensionAlias) {
                    if(
                        file_exists(
                            $ConfigurationFile = "$ConfigurationDirectory/$ExtensionAlias.yml"
                        )
                    ) {
                        $callback(
                            $ExtensionAlias, Yaml::parse($ConfigurationFile)
                        );
                    }
                }
            }
        }
    }

}
