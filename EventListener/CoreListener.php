<?php

namespace ThinkCreative\CoreBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use ThinkCreative\CoreBundle\Services\RequestViewHandler;

class CoreListener implements EventSubscriberInterface
{

    protected $RequestViewHandler;

    public function __construct(RequestViewHandler $request_view_handler) {
        $this->RequestViewHandler = $request_view_handler;
    }

    public function onController(FilterControllerEvent $event) {
        $this->RequestViewHandler->isMasterRequest(
            $isMasterRequest = $event->getRequestType() == HttpKernelInterface::MASTER_REQUEST
        );

        if($isMasterRequest) {
            $this->RequestViewHandler->processController(
                $event->getController()
            );
        }
    }

    public static function getSubscribedEvents() {
        return array(
            KernelEvents::CONTROLLER => array('onController'),
        );
    }

}
